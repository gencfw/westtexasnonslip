<?php /* Template Name: Specials Page*/ ?>
<?php get_header(); ?>
    <div class="index-main-section center">
        <div class="main-filter center">
            <div class="main-container">
                <img class="main-image" src="<?php bloginfo('template_directory'); ?>/img/west%20texas%20logo%20hero.png">
                <h1 class="main-slogan center">Your solution to unsafe dry/wet slippery floors</h1>
            </div>
        </div>
    </div>
    <div class="sure-step-accent center">
        <p>A piece of mind at a great price</p>
    </div>
    <div class="main-body">
        <div class="main-body-container">
            <div class="body-left center">
                <img src="<?php bloginfo('template_directory'); ?>/img/c289e00896a79cc9ea5666144141ca30.jpg" class="special-image">
            </div>
            <div class="body-right">
                <p>Slips and falls in the bathrub are a common occurence, with more than 157,000 people going to an emergency room a year from bathtub falls. Injuries are a commom occurence with people of age 65+ being substantially more susceptible to slip and falls.</p>
            </div>
        </div>
    </div>
    <div class="main-body">
        <div class="main-body-container">
            <div class="body-right center body-hidden">
                <a href="tel:1-915-328-8000"><img src="<?php bloginfo('template_directory'); ?>/img/bathtub_clear_coat_promo.jpg" class="special-image-main"></a>
            </div>
            <div class="body-left">
                <p>Currently we are offering a special on our invisible bathtub treatment. Our Sure Step treatment will make your bathtub safer, GUARANTEED, for a limited time price of <strong>$39.99</strong>. Greatly decrease the risk of faling in your bathtub without the need of any extra equipment. Call us today to schedule an appointment.</p>
                <p style="font-weight: bold;">This sale is only for a limited time so be sure to take advantage of it before it's gone.</p>
            </div>
            <div class="body-right body-desktop center">
                <a href="tel:1-915-328-8000"><img src="<?php bloginfo('template_directory'); ?>/img/bathtub_clear_coat_promo.jpg"></a>
            </div>
        </div>
    </div>
    <?php get_sidebar(); ?>
    <?php get_footer(); ?>
