<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>
        <?php echo get_bloginfo( 'name' ); ?>
    </title>
    <meta name="description" content="<?php echo get_bloginfo( 'description' ); ?>" />
    <meta name="geo.region" content="US-TX" />
    <meta name="geo.placename" content="el paso" />
    <meta name="geo.position" content="37.09024;-95.712891" />
    <meta name="ICBM" content="37.09024, -95.712891" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" />
    <?php wp_head(); ?>
</head>

<body>

    <div class="mobile-header center"> <img alt="Menu Icon" class="mobile-nav" src="<?php bloginfo('template_directory'); ?>/img/mobile_nav.png">
        <h2 class="mobile-nav-title">
            <?php the_title_attribute(); ?>
        </h2>
    </div>
    <div class="nav center desktop-only" id='side_panel'>
        <img class="mobile-close-button mobile-only" src="<?php bloginfo('template_directory'); ?>/img/mobile_close.png" id="close_button" alt="Close button" />

        <img class="nav-logo" src="<?php bloginfo('template_directory'); ?>/img/west%20texas%20logo%20hero.png">

        <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'menu_class' => 'nav-ul', 'container' => false ) ); ?>

    </div>
    <div class="index-main-section center red-bot">
        <div class="main-filter center">
            <div class="main-container">
                <img class="main-image" src="http://westtexasnonslip.com/wp-content/uploads/2017/04/west-texas-logo-hero-1024x206.png" alt="" />
                <h2 class="main-slogan center">Keeping You On Your Feet</h2>
                <h2 class="main-slogan center" style="padding-left: 10px;">     Preventing Injuries &amp; Lawsuits</h2>
            </div>
        </div>
    </div>
