<?php
?>
<div class="call-button red-bot-top" style="">
    <h3>Call Today for a FREE demonstration and estimate</h3>
    <h3><a href="tel:1-915-328-8000">Office: 915-328-8000</a></h3>
    <h3><a href="tel:1-915-479-8282">Cell: 915-479-8282</a></h3>
    <h3><a class="mobile-email" href="mailto:westtexasnonslip@gmail.com?" target="_top">Email: westtexasnonslip@gmail.com</a></h3>
</div>
<div class="footer center">
    <div class="footer-content">
        <div class="footer-social-media">
            <div class="footer-seal">
                <a href="#"><img class="seal-icon" src="<?php bloginfo('template_directory'); ?>/img/cve.bmp"></a>
            </div>
            <div class="footer-seal">
                <a href="#"><img class="seal-icon" src="<?php bloginfo('template_directory'); ?>/img/bbb.jpg"></a>
            </div>
            <div class="ffooter-seal">
                <a href="#"><img class="seal-icon" src="<?php bloginfo('template_directory'); ?>/img/hispanic.jpg"></a>
            </div>
            <div class="ffooter-seal">
                <a href="#"><img class="seal-icon" src="<?php bloginfo('template_directory'); ?>/img/texas-hub.png"></a>
            </div>
        </div>
        <div class="footer-text">
            <p>Created by <span style="font-weight: 700"><a href="http://unthink.me/"></a>Unthink</span></p>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
</body>

</html>
