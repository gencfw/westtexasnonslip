<?php /* Template Name: Homepage Page*/ ?>
<?php get_header(); ?>
<div class="index-main-section center red-bot">
        <div class="main-filter center">
            <div class="main-container">
                <img class="main-image" src="<?php bloginfo('template_directory'); ?>/img/west%20texas%20logo%20hero.png">
                <h1 class="main-slogan center">Your solution to unsafe dry/wet slippery floors</h1>
            </div>
        </div>
    </div>
    <div class="main-body">
        <div class="main-body-container">
            <div class="body-full">
                <p>West Texas Non-Slip is your local solution to slippery and hazardous floors in the El Paso and southwest region. We specialize in Sure Step treatments that are invisible once applied and immediately start preventing falls. Call us today at 915-328-800 or 915-479-8282 to schedule a free in person demonstration of our product.</p>
            </div>
        </div>
    </div>
    <div class="main-body">
        <div class="main-body-container">
            <div class="body-left center">
                <h1 style="color: #D30202">Caution</h1>
                <p style="font-size: 28px;">Is your bathtub/floors safe?</p>
            </div>
            <div class="body-right">
                <p>Slips and falls are the #1 cause of Accidental injuries in hotels, restaurants and public buildings with approximately 25,000 people daily going to hospitals from related injuries. The average cost to defend a slip and fall lawsuit is $50,000. West Texas Non Slip is your answer for non slippery commercial flooring. There is no better way to achieve non slippery floors than SURE STEP.</p>
            </div>
        </div>
    </div>
    <div class="main-body">
        <div class="main-body-container">
            <div class="body-right center body-hidden">
                <a href="http://westtexasnonslip.com/sure-step"><img src="<?php bloginfo('template_directory'); ?>/img/sure%20step%20el%20paso%20texas%20logo_main_gray.png"></a>
                <!--                <h1>attention business owner</h1>-->
            </div>
            <div class="body-left">
                <p>After every wash, mop and rain the floors of your office building and store become extra slippery and dangerous for your guests and for your bottom line. Don't risk having accidents and law suits because of an overlook. -- Our product SURE STEP is a clear treatment that is easily applied over your surface to increase a surface's co-efficient of friction by 200-400% to avoid risk of falling accidents. </p>
            </div>
            <div class="body-right body-desktop center">
                <!--                <h1>attention business owner</h1>-->
                <a href="http://westtexasnonslip.com/sure-step"><img src="<?php bloginfo('template_directory'); ?>/img/sure%20step%20el%20paso%20texas%20logo_main_gray.png"></a>
            </div>
        </div>
    </div>
    <div class="main-body">
        <div class="main-body-container">
            <div class="body-left center">
                <h1>Our product makes floors safer for the following</h1>
            </div>
            <div class="body-right">
                <ul>
                    <li>Guests/Visitors... With certain steps and obstacles an unfamiliar environment could cause difficulty with slipping and falling.</li>
                    <li>Elderly... Slower reactions can be an unfortunate cause of many slip/fall accidents, particularly when walking on a wet floor.</li>
                    <li>Handicapped... Restricted movement or vision can result in the increased possibility of a fall.</li>
                    <li>Everyone... We've all experienced a slip at some time or another. And it always comes as a surprise... we can never seem to be too careful.</li>
                </ul>
            </div>
        </div>
    </div>
    <?php get_sidebar(); ?>
    <?php get_footer(); ?>
