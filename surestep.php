<?php /* Template Name: SureStep Page*/ ?>
<?php get_header(); ?>
    <div class="index-main-section center">
        <div class="main-filter center">
            <div class="main-container">
                <img class="main-image" src="<?php bloginfo('template_directory'); ?>/img/west%20texas%20logo%20hero.png">
                <h1 class="main-slogan center">Your solution to unsafe dry/wet slippery floors</h1>
            </div>
        </div>
    </div>
    <div class="sure-step-accent center">
        <p>What is sure step?</p>
    </div>
    <div class="sure-step-text">
        <img src="<?php bloginfo('template_directory'); ?>/img/sure%20step%20el%20paso%20texas%20logo_main.png">
        <p>A 20 year company that is fast gaining ground worldwide. Sure Step treatment is a unique, uncommon long lasting treatment that consistently works, leaving our customers satisfied. Sure Step has been scientifically tested and proven to work and is treatment friendly. When the treated surfaces, like a hard mineral floor or bathtubs are subjected to water. Sure Step is proven to increase the coefficient of friction by 200-400%. Creating an INVISIBLE sophisticated tread. The surfaces become safer to walk on, GUARANTEE!</p>
        <p style="font-weight: bold;">Sure Step is not a coating, not a film, not a spray, but an invisible friendly treatment.</p>

    </div>
    <div class="sure-step-accent center">
        <p>Developed and proven for</p>
    </div>
    <div class="sure-step-science">
        <p>Sure Step has been proven to work for the following:</p>
        <ul>
            <li>Bathubs, Showers, and Bathrooms.</li>
            <li>Commercial and Residential Kitchens</li>
            <li>Swimming Pools</li>
            <li>Concrete, Spanish Tile, Quarry, Tile, Terrazzo, Ceramic Tile</li>
            <li>Porcelain/Enamel, Marble</li>
            <li>And many other hard mineral surfaces (tiles)</li>
        </ul>
        <p>The result of our products are INVISIBLE and IMMEDIATE therefore we always recommend a complete, in person demonstration of our product.</p>
        <img class="desktop-chart" src="<?php bloginfo('template_directory'); ?>/img/chart.jpg">
        <img class="mobile-chart" src="<?php bloginfo('template_directory'); ?>/img/chart-1.jpg">
        <img class="mobile-chart" src="<?php bloginfo('template_directory'); ?>/img/chart-2.jpg">
    </div>
    <?php get_sidebar(); ?>
    <?php get_footer(); ?>
