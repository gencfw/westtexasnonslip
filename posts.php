<?php /* Template Name: Blog Posts Page*/ ?>
<?php get_header(); ?>
<div class="blog-body">
    <h2 class="blog-page-title">Posts to keep you on your feet</h2>
    <br/>
    <div class="main-body-container">
        <?php 

           		$args = array();
        
                $postslist = get_posts( $args );    

                foreach ($postslist as $post) :  setup_postdata($post); ?>
        <div class="post-flex-item">
            <div class="post-flex-content">
                <div class="center">
                    <a style="text-decoration: none;" href="<?php the_permalink(); ?>">
                    <img style="max-height: 250px; max-width: 100%;" src="<?php 
                                                 if ( has_post_thumbnail('medium') ) {
                                                    the_post_thumbnail();
                                                 } else {
                                                      echo( get_template_directory_uri() . '/img/post-star.png');
                                                 } ?>">
                </div>
                
                    <h5 style="margin-top:3vh;">
                        <?php the_title(); ?>
                    </h5>
                </a>
                    <?php the_excerpt(); ?>
                </div>
            </div>
            <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
        </div>
    </div>
    <?php get_sidebar(); ?>
    <?php get_footer(); ?>
