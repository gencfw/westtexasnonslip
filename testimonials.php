<?php /* Template Name: Testimonials Page*/ ?>
<?php get_header(); ?>
    <div class="index-main-section center">
        <div class="main-filter center">
            <div class="main-container">
                <img class="main-image" src="<?php bloginfo('template_directory'); ?>/img/west%20texas%20logo%20hero.png">
                <h1 class="main-slogan center">Your solution to unsafe dry/wet slippery floors</h1>
            </div>
        </div>
    </div>
    <div class="sure-step-accent center">
        <p>Comments from our customers</p>
    </div>
    <div class="quotes-container">
        <blockquote class="example-right">
            <p>"Just a quick note to tell you your Sure Step Non-Slip is still working great. Our floors are as safe as they where when the product was first applied."</p>
        </blockquote>
        <p>Restaurant Chain</p>
        <blockquote class="example-right">
            <p>"I have never seen anything like it. Our kitchen floor was a disaster...the staff were falling all the time. I didn't believe that your Non-Slip would make such a difference, but the floors are no longer dangerous. Thank You."</p>
        </blockquote>
        <p>Restaurant Chain</p>
        <blockquote class="example-right">
            <p>"As I told you, we had tile down for three days before we had our first slip and fall accident...we have not had a slip and fall accident since you applied the Sure Step"</p>
        </blockquote>
        <p>Condominium Complex</p>
        <blockquote class="example-right">
            <p>"Our bathtubs always had bathmats until we were introduced to Sure Step Non-Slip. Now all our bathtubs are treated with your remarkable product."</p>
        </blockquote>
        <p>Hotel Chain</p>
    </div>
    <div style="background-color: #EEEEEE;" class="sure-step-accent center">
        <p style="color: black;">Still Not conviced?</p>
    </div>
    <?php get_sidebar(); ?>
    <?php get_footer(); ?>
